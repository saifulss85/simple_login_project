<?php
use View\View;

require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";

SessionManager::startSessionIfNotStarted();
//SessionManager::printVariablesKeyVal();
if (!isset($_SESSION[Constants::SESSION_TAG_IS_LOGGED_IN_SUCCESSFULLY])) {
    header("location: login.php");
}

/** @var $user User */
$user = SessionManager::getSessionVariable(Constants::SESSION_TAG_USER);
?>
<!DOCTYPE html>
<html>
<head>
    <?php View::getInstance()->echoHtmlHead(Constants::PROJECT_TITLE) ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php View::getInstance()->echoHeader($user) ?>
    <?php View::getInstance()->echoSidebar($user) ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dashboard
                <small>Your vital statistics at a glance.</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2>Chart 1</h2>
                        <p>Some chart goes here.</p>
                        <p>
                            <a class="btn btn-default" href="#" role="button">
                                View details &raquo;
                            </a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <h2>Chart 2</h2>
                        <p>Some chart goes here.</p>
                        <p>
                            <a class="btn btn-default" href="#" role="button">
                                View details &raquo;
                            </a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <h2>Chart 3</h2>
                        <p>Some chart goes here.</p>
                        <p>
                            <a class="btn btn-default" href="#" role="button">
                                View details &raquo;
                            </a>
                        </p>
                    </div>
                </div>
                <hr>
            </div>
        </section>
    </div>

    <?php View::getInstance()->echoFooter() ?>
    <?php View::getInstance()->echoControlSidebar() ?>
</div>
</body>
</html>

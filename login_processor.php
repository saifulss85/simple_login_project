<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";
SessionManager::markActivity();

$username = PostManager::getInstance()->getPostVariable("username");
$password = PostManager::getInstance()->getPostVariable("password");

if (AuthManager::getInstance()->isLoginLegit($username, $password)) {
    $user = User::getUser($username);

    if (is_null($user)) {
        header("Location: login.php?" . Constants::GET_TAG_ERROR_TYPE . "=" . Constants::GET_TAGVALUE_ERROR_TYPE_VALUE_NO_ACCESS);
    } else {
        SessionManager::startSessionIfNotStarted();
        SessionManager::setSessionVariable(Constants::SESSION_TAG_USER, $user);
        SessionManager::setSessionVariable(Constants::SESSION_TAG_IS_LOGGED_IN_SUCCESSFULLY, true);
        header("Location: index.php");
    }
} else {
    header("Location: login.php?" . Constants::GET_TAG_ERROR_TYPE . "=" . Constants::GET_TAGVALUE_ERROR_TYPE_VALUE_INVALID_CREDENTIALS);
}

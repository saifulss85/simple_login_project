<?php
use View\View;

require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";
SessionManager::startSessionIfNotStarted();
SessionManager::unsetAll();
SessionManager::markActivity();

try {
    $errorType = GetManager::getInstance()->getGetVariable(Constants::GET_TAG_ERROR_TYPE);
} catch (Exception $e) {
    $errorType = null;
} finally {
    //do nothing
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php View::getInstance()->echoHtmlHead(Constants::PROJECT_TITLE); ?>
</head>
<body>
<div id="login-wrapper">
    <div class="container">
        <div class="login-panel">
            <div class="row">
                <div class="col-md-12 login-heading-div">
                    <h3 class="login-heading"><?php Constants::PROJECT_TITLE ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="login_processor.php" method="post" novalidate>
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="PUBNET Username"
                                               name="username"
                                               type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="PUBNET Password"
                                               name="password"
                                               type="password" value="">
                                    </div>
                                    <button class="btn btn-lg btn-primary btn-block">Login</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            switch ($errorType) {
                case Constants::GET_TAGVALUE_ERROR_TYPE_VALUE_INVALID_CREDENTIALS:
                    ?>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                PUBNET credentials invalid. Call 6731 3666 for AFM Service Desk and
                                request assistance
                                to reset PUBNET ID. Alternatively, email them at
                                afm_ncs_servicedesk@support.gov.sg.
                                <button type="button" class="close" data-dismiss="alert">
                                    <span>&times;</span></button>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                case Constants::GET_TAGVALUE_ERROR_TYPE_VALUE_NO_ACCESS:
                    ?>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                Your PUBNET credentials are valid. But you don't have access to
                                PCMS. Please contact
                                system administrator Marc Ong at marc_ong@pub.gov.sg.
                                <button type="button" class="close" data-dismiss="alert">
                                    <span>&times;</span></button>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                case null:
                    //do nothing
                    break;
                default:
                    ?>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                If you're seeing this, something went badly wrong. The default case
                                should never show.
                                Now go round the halls running naked with your arms flailing around
                                yelling "YEGADS!"
                                <button type="button" class="close" data-dismiss="alert">
                                    <span>&times;</span></button>
                            </div>
                        </div>
                    </div>
                    <?php
            }

            if ($errorType == Constants::GET_TAGVALUE_ERROR_TYPE_VALUE_INVALID_CREDENTIALS) {
                ?>

                <?php
            } ?>
        </div>
    </div>
</div>
</body>
</html>
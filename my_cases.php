<?php
use View\View;

require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";

SessionManager::startSessionIfNotStarted();
//SessionManager::printVariablesKeyVal();
if (!isset($_SESSION[Constants::SESSION_TAG_IS_LOGGED_IN_SUCCESSFULLY])) {
    header("location: login.php");
}

/** @var $user User */
$user = SessionManager::getSessionVariable(Constants::SESSION_TAG_USER);
?>
<!DOCTYPE html>
<html>
<head>
    <?php View::getInstance()->echoHtmlHead(Constants::PROJECT_TITLE) ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php View::getInstance()->echoHeader($user) ?>
    <?php View::getInstance()->echoSidebar($user) ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                My Cases
                <small>A listing of all your current open cases.</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Current Open Cases</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Item</th>
                                <th>Status</th>
                                <th>Popularity</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                <td>Call of Duty IV</td>
                                <td><span class="label label-success">Shipped</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                <td>Samsung Smart TV</td>
                                <td><span class="label label-warning">Pending</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                <td>iPhone 6 Plus</td>
                                <td><span class="label label-danger">Delivered</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                <td>Samsung Smart TV</td>
                                <td><span class="label label-info">Processing</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00c0ef" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                <td>Samsung Smart TV</td>
                                <td><span class="label label-warning">Pending</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                <td>iPhone 6 Plus</td>
                                <td><span class="label label-danger">Delivered</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                <td>Call of Duty IV</td>
                                <td><span class="label label-success">Shipped</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">
                                        <canvas width="34" height="20"
                                                style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Create
                        New Case</a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View
                        All Cases</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </section>
    </div>

    <?php View::getInstance()->echoFooter() ?>
    <?php View::getInstance()->echoControlSidebar() ?>
</div>
</body>
</html>

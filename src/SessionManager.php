<?php

/**
 * Class SessionManager
 */
class SessionManager
{
    const TAG_LAST_ACTIVITY = "LAST_ACTIVITY";

    /**
     * @return bool
     */
    private static function isSessionStarted()
    {
        return (session_id() == '') ? false : true;
    }

    public static function printVariablesKeyVal()
    {
        if (count($_SESSION) == 0) {
            echo "there are no session variables";
        } else {
            foreach ($_SESSION as $key => $val) {
                $type = gettype($val);
                if ($type == "object") {
                    $val = print_r($val, true);
                }
                echo $key . ": " . $type . " with value " . $val . "<br/>";
            }
        }
    }

//    public static function printVariablesKeyOnly()
//    {
//        if (count($_SESSION) == 0) {
//            echo "there are no session variables";
//        } else {
//            foreach ($_SESSION as $key => $val) {
//                $type = gettype($val);
//                echo $key . ": " . $type . "<br/>";
//            }
//        }
//    }

    /**
     * @return bool
     */
    public static function startSessionIfNotStarted()
    {
        if (self::isSessionStarted()) {
            return false;
        } else {
            return session_start();
        }
    }

    public static function markActivity()
    {
        self::setSessionVariable(self::TAG_LAST_ACTIVITY, time());
    }

    /**
     * @param $variableName string
     * @param $variableValue string
     */
    public static function setSessionVariable($variableName, $variableValue)
    {
        $_SESSION[$variableName] = $variableValue;
    }

    /**
     * @param $variableName string
     * @return string
     */
    public static function getSessionVariable($variableName)
    {
        return $_SESSION[$variableName];
    }

    public static function unsetAll()
    {
        session_unset();
    }

    public static function displayServerGlobals()
    {
        $indicesServer = [
            'PHP_SELF',
            'argv',
            'argc',
            'GATEWAY_INTERFACE',
            'SERVER_ADDR',
            'SERVER_NAME',
            'SERVER_SOFTWARE',
            'SERVER_PROTOCOL',
            'REQUEST_METHOD',
            'REQUEST_TIME',
            'REQUEST_TIME_FLOAT',
            'QUERY_STRING',
            'DOCUMENT_ROOT',
            'HTTP_ACCEPT',
            'HTTP_ACCEPT_CHARSET',
            'HTTP_ACCEPT_ENCODING',
            'HTTP_ACCEPT_LANGUAGE',
            'HTTP_CONNECTION',
            'HTTP_HOST',
            'HTTP_REFERER',
            'HTTP_USER_AGENT',
            'HTTPS',
            'REMOTE_ADDR',
            'REMOTE_HOST',
            'REMOTE_PORT',
            'REMOTE_USER',
            'REDIRECT_REMOTE_USER',
            'SCRIPT_FILENAME',
            'SERVER_ADMIN',
            'SERVER_PORT',
            'SERVER_SIGNATURE',
            'PATH_TRANSLATED',
            'SCRIPT_NAME',
            'REQUEST_URI',
            'PHP_AUTH_DIGEST',
            'PHP_AUTH_USER',
            'PHP_AUTH_PW',
            'AUTH_TYPE',
            'PATH_INFO',
            'ORIG_PATH_INFO'
        ];

        echo '<table cellpadding="10">';
        foreach ($indicesServer as $arg) {
            if (isset($_SERVER[$arg])) {
                echo '<tr><td>' . $arg . '</td><td>' . $_SERVER[$arg] . '</td></tr>';
            } else {
                echo '<tr><td>' . $arg . '</td><td>-</td></tr>';
            }
        }
        echo '</table>';
    }

    /**
     * @param int $timeoutLengthInMinutes
     */
    public static function expireSessionIfExceedTimeout($timeoutLengthInMinutes)
    {
        $lastActivityTimestamp = intval(self::getSessionVariable(self::TAG_LAST_ACTIVITY));
        $comparisonTimestamp = $lastActivityTimestamp + $timeoutLengthInMinutes * 60;
        if ($comparisonTimestamp < time()) {
            self::unsetAll();
        }
    }
}
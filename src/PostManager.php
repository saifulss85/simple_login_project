<?php

/**
 * Class PostManager
 */
class PostManager
{
    /** @var PostManager */
    private static $instance;

    /**
     * PostManager constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return PostManager
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new PostManager();
        }
        return self::$instance;
    }

    public function printVariablesKeyVal()
    {
        if (count($_POST) == 0) {
            echo "there are no POST variables";
        } else {
            foreach ($_POST as $key => $val) {
                $type = gettype($val);
                if ($type == "object") {
                    $val = "[objects cannot be stringified]";
                }
                echo $key . ": " . $type . " with value " . $val . "<br/>";
            }
        }
    }

    /**
     * @param $varName string
     * @return string
     * @throws Exception
     */
    public function getPostVariable($varName)
    {
        if (isset($_POST[$varName])) {
            return $_POST[$varName];
        } else {
            throw new Exception('POST variable "' . $varName . '" not set.');
        }
    }

//    /**
//     * @param $varName string
//     * @return bool
//     */
//    public function isPostVariableSet($varName)
//    {
//        if (isset($_POST[$varName])) {
//            return true;
//        } else {
//            return false;
//        }
//    }

//    public function displayPostContents()
//    {
//        echo '<table cellpadding="10">';
//        foreach ($_POST as $arg) {
//            if (isset($_POST[$arg])) {
//                echo '<tr><td>' . $arg . '</td><td>' . $_POST[$arg] . '</td></tr>';
//            } else {
//                echo '<tr><td>' . $arg . '</td><td>-</td></tr>';
//            }
//        }
//        echo '</table>';
//    }
}
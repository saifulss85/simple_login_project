<?php

/**
 * Class GetManager
 */
class GetManager
{
    /** @var GetManager */
    private static $instance;

    /**
     * GetManager constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return GetManager
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new GetManager();
        }
        return self::$instance;
    }

    public function printVariablesKeyVal()
    {
        if (count($_GET) == 0) {
            echo "there are no GET variables";
        } else {
            foreach ($_GET as $key => $val) {
                $type = gettype($val);
                if ($type == "object") {
                    $val = "[objects cannot be stringified]";
                }
                echo $key . ": " . $type . " with value " . $val . "<br/>";
            }
        }
    }

    /**
     * @param $varName string
     * @return string
     * @throws Exception
     */
    public function getGetVariable($varName)
    {
        if (isset($_GET[$varName])) {
            return $_GET[$varName];
        } else {
            throw new Exception('GET variable "' . $varName . '" not set.');
        }
    }
}
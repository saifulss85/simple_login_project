<?php

/**
 * Class AuthManager
 */
class AuthManager
{
    /** @var AuthManager */
    private static $instance;

    /**
     * AuthManager constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return AuthManager
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new AuthManager();
        }
        return self::$instance;
    }


    /**
     * @param $username string
     * @param $password string
     * @return bool
     */
    public function isLoginLegit($username, $password)
    {
        //TODO: check database if username/password is legit
        return true;
    }
}
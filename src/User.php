<?php

/**
 * Class User
 */
class User
{
    /** @var string */
    private $username;

    /** @var string */
    private $email;

    /** @var string */
    private $imageName;

    /** @var string[] */
    private $personas = [];

    /**
     * User constructor.
     * @param string $username
     * @param string $email
     * @param string $imageName
     * @param string[] $personas
     */
    public function __construct($username, $email, $imageName, array $personas)
    {
        $this->username = $username;
        $this->email = $email;
        $this->imageName = $imageName;
        $this->personas = $personas;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @return string[]
     */
    public function getPersonas()
    {
        return $this->personas;
    }

    /**
     * @param $username string
     * @return null|User
     */
    public static function getUser($username)
    {
        switch ($username) {
            case "raihan":
                return new User($username, "raihan@simplelogin.com", "raihan.jpg", ["normal"]);
                break;
            case "saiful":
                return new User($username, "saiful@simplelogin.com", "saiful.jpg", ["normal"]);
                break;
            case "admin":
                return new User($username, "admin@simplelogin.com", "admin.jpg", ["admin", "normal"]);
                break;
            default:
                return null;
        }
    }
}
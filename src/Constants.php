<?php

class Constants
{
    const PROJECT_TITLE = "Simple Login Project";
    const GET_TAG_ERROR_TYPE = "error_type";
    const GET_TAGVALUE_ERROR_TYPE_VALUE_INVALID_CREDENTIALS = "invalid_credentials";
    const GET_TAGVALUE_ERROR_TYPE_VALUE_NO_ACCESS = "no_access";
    const SESSION_TAG_USER = "user";
    const SESSION_TAG_IS_LOGGED_IN_SUCCESSFULLY = "isLoggedInSuccessfully";
}
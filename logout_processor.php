<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor/autoload.php";
SessionManager::startSessionIfNotStarted();
SessionManager::unsetAll();
header("Location: login.php");